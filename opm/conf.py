# -*- mode:python; coding:utf-8; tab-width:4 -*-

from pyarco.Type import DictAttrDeco, DictPathDeco, load_module_as_dict

from opm import default_config


class Settings(dict):
    pass


default = load_module_as_dict(default_config)
settings = DictPathDeco(DictAttrDeco(Settings(default)))
