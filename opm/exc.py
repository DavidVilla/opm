# -*- coding:utf-8; tab-width:4; mode:python -*-

class MissingMandatoryField(Exception): pass
class UnknownField(Exception): pass
class CastingError(Exception): pass

class MissingSchema(Exception): pass
class DuplicatePrimaryKey(Exception): pass
