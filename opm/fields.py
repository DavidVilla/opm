# -*- coding:utf-8; tab-width:4; mode:python -*-

import copy

import exc

class Field(object):
    creation_counter = 0
    cast = None
    default = None
    related = False

    @classmethod
    def get_counter(cls):
        retval = Field.creation_counter
        Field.creation_counter += 1
        return retval

    valid_options = dict(
        auto = False,
        primary_key = False,
        default = None,
        opt = False,
        )


    def __init__(self, **kargs):
        self.order = self.get_counter()

        unknown_options = set(kargs.keys()) - set(self.valid_options.keys())
        if unknown_options:
            raise TypeError("{0} got unexpected keyword arguments: {1}".format(
                    self.__class__.__name__, list(unknown_options)))

        if not 'default' in kargs.keys():
            kargs['default'] = self.__class__.default

        for key in self.valid_options.keys():
            setattr(self, key, kargs.get(key, self.valid_options[key]))

        if self.auto:
            self.opt = True

    def to_python(self, value):
        raise NotImplementedError(self.__class__)

    def to_store(self, value):
        raise NotImplementedError(self.__class__)

    def fallback(self):
        if self.opt:
            return copy.copy(self.default)

        raise exc.MissingMandatoryField


    def __cmp__(self, other):
        return cmp(self.order, other.order)

    def __unicode__(self):
        return "{0}:{1}".format(self.order, self.__class__.__name__)

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __repr__(self):
        return "<{0}>".format(unicode(self))


class SingleField(Field):
    def to_python(self, value):
        assert self.cast is not None, "You must use a Field subclass"

        if isinstance(value, self.cast):
            return value

        try:
            return self.cast(value)
        except (ValueError), e:
            raise exc.CastingError("Can not cast value '{0}' to type '{1}'".format(
                    repr(value), self.cast.__name__))
        except TypeError, e:
            raise TypeError("{0} {1}: {2}".format(self, self.cast, e))

    def to_store(self, value):
        assert isinstance(value, self.cast), "'{0}' should be {1}".format(value, self.cast)
        return str(value)


class TextField(SingleField):
    cast = str
    default = u''

class IntField(SingleField):
    cast = int
    default = 0


class AutoField(IntField):
    def __init__(self, *kargs):
        super(IntField, self).__init__(primary_key=True, auto=True)

    def to_python(self, value):
        if value is None:
            return value

        return super(AutoField, self).to_python(value)

    def to_store(self, value):
        return str(value)


class OneToOneField(Field):
    related = True

    def __init__(self, model, **kargs):
        self.cast = model
        super(OneToOneField, self).__init__(**kargs)

    def to_python(self, value):
        return value


class ForeignField(Field):
    related = True

    def __init__(self, model, **kargs):
        self.cast = model
        Field.__init__(self, **kargs)

    def to_store(self, value):
        return "<foreign>"


class OneToManyField(Field):
    related = True

    def __init__(self, model, **kargs):
        self.cast = model
        kargs['auto'] = True
        kargs['default'] = []
        super(OneToManyField, self).__init__(**kargs)



    # FIXME!!
    def to_python(self, value):
        if isinstance(value, list):
            return value

        if isinstance(value, str):
            return value.split(',')

        return None

    def to_store(self, value):
        assert isinstance(value, list)
        return value


class ManyToManyField(Field):
    related = True

    def __init__(self, model, **kargs):
        self.cast = model
        kargs['auto'] = True
        kargs['default'] = []
        super(ManyToManyField, self).__init__(**kargs)

    def to_python(self, value):
        assert isinstance(value, list), value
        for i in value:
            assert isinstance(i, (self.cast, str)), i
        return value

    def to_store(self, value):
        assert isinstance(value, list)
        return value
