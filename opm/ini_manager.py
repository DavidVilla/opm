# -*- coding:utf-8; tab-width:4; mode:python -*-

import pyarco.iniparser
from pyarco.Type import striplit

from manager import Manager, FileManager, UndefinedValue
from fields import OneToOneField, OneToManyField


class IniParser(pyarco.iniparser.DictConfigParser):
    default_section = 'MAIN'


class IniManager(FileManager):

    def _store_create(self, fpath=None):
        retval = IniParser()
        if fpath:
            retval.readfp(self.FS.open(fpath))
        return retval

    def _store_save(self, store, fpath):
        with self.FS.open(fpath, 'w') as fd:
            store.write(fd)

    def _load_1to1_related(self, key, field, store, subject):
        if not store.has_section(key):
            return UndefinedValue(key)

        return field.cast(**store.get_section(key))

    def _load_1toM_related(self, key, field, store, subject):
        retval = []

        for pk in striplit(store[key], sep=','):
            instance = field.cast(**store.get_section(pk))
            instance.set_pk(pk)
            retval.append(instance)

        return retval

    def _load_MtoM_related(self, key, field, store, subject):
        pass  # FIXME

    def _save_value(self, key, field, store, value):
        store[key] = field.to_store(value)

    def _save_1to1_related(self, key, field, store, subject):
        if subject is None:
            return

        subject.validate()
        self._save_items(store.new_section(key), subject)

    def _save_1toM_related(self, key, field, store, instances):
        store[key] = str.join(', ', [x.pk for x in instances])
        for subject in instances:
            self._save_items(store.new_section(subject.pk), subject)

    def _save_MtoM_related(self, key, field, store, subject):
        pass  # FIXME
