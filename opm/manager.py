# -*- coding:utf-8; tab-width:4; mode:python -*-

import os

from fields import OneToOneField, OneToManyField, ManyToManyField
import opm.exc as exc

class UndefinedValue(object):
    def __init__(self, key):
        self.key = key


class Manager(object):

    managers = {}

    @classmethod
    def setup_fs(cls, fs):
        cls.FS = fs

    @classmethod
    def register(cls):
        cls.managers[cls.__name__] = cls

    def __init__(self, model):
        assert isinstance(model, type)
        self.model = model
        self.table = "%ss" % self.model.__name__.lower()

    def load(self, pk):
        raise NotImplementedError

    def all(self):
        raise NotImplementedError

    def save(self, subject):
        raise NotImplementedError


class FileManager(Manager):
    def _pk_to_path(self, pk):
        return os.path.join(self.table, '{0}.ini'.format(pk))

    def _path_to_pk(self, path):
        return os.path.splitext(os.path.split(path)[1])[0]

    def load(self, pk):
        store = self._store_create(self._pk_to_path(pk))
        retval = self.model(**store)
        self._load_related(store, retval)
        setattr(retval, self.model.schema.primary_key, pk)

        retval._store = store
        retval.validate()
        return retval

    def _load_related(self, store, subject):
        hooks = {
            OneToOneField:   self._load_1to1_related,
            OneToManyField:  self._load_1toM_related,
            ManyToManyField: self._load_MtoM_related,
            }

        for key, field in subject.schema.items():
            if not field.related:
                continue

            hook = hooks[field.__class__]
            setattr(subject, key, hook(key, field, store, subject))

    def all(self):
        return [self.load(self._path_to_pk(x)) for x in
                          self.FS.listdir(self.table)]

    def save(self, subject):
        fpath = self._pk_to_path(subject.pk)

        if not subject._store:
            if self.FS.exists(fpath):
                raise exc.DuplicatePrimaryKey
            else:
                self.FS.mkdir(self.table)

            subject._store = self._store_create()

        conn = subject._store
        self._save_items(conn, subject)
        self._store_save(conn, fpath)

    def _save_items(self, store, subject):
        hooks = {
            OneToOneField:   self._save_1to1_related,
            OneToManyField:  self._save_1toM_related,
            ManyToManyField: self._save_MtoM_related,
            }

        for key, field in subject.schema.items():
            if field.primary_key:
                continue

            item = getattr(subject, key)
            hook = hooks.get(field.__class__, self._save_value)
            hook(key, field, store, item)


class RelatedManager(object):
    def __init__(self, model, related, attr):
        self.model = model
        self.related = related
        self.attr = attr
        self.instances = []
        setattr(related, attr, self.instances)

    def create(self, **kargs):
        retval = self.model(**kargs)
        self.instances.append(retval)
        return retval


import ini_manager, obj_manager
ini_manager.IniManager.register()
obj_manager.ObjManager.register()
