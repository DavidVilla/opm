# -*- coding:utf-8; tab-width:4; mode:python -*-

from inspect import getmembers
from operator import itemgetter

import pyarco.fs

try:
    from collections import OrderedDict
except ImportError:
    from pyarco.Type import SortedDict as OrderedDict


from fields import Field, AutoField, OneToOneField, ForeignField, OneToManyField
import exc

from manager import Manager, RelatedManager, UndefinedValue
from conf import settings


#def getattributes(instance):
#    return [item for item in inspect.getmembers(instance, lambda x:not callable(x))
#            if not item[0].startswith('__')]
#
#def attr_names(attrs):
#    return [x[0] for x in attrs]


def field_names(dct):
    return set(dct.keys())



#class Register(dict):
#    def __init__(self):
#        dict.__init__(self, dict(getattributes(self)))
#
#    def __setattr__(self, key, value):
#        self[key] = value


class Schema(OrderedDict):

    class MissingPrimaryKey(Exception): pass
    class MultiplePrimaryKeys(Exception): pass

    def __init__(self, model_fields=[]):
        if not model_fields:
            raise exc.MissingSchema()

        OrderedDict.__init__(self, model_fields)

        try:
            pk = self.__get_primary_key()
        except self.MissingPrimaryKey:
            self['id'] = AutoField()
            pk = 'id'

        self.primary_key = pk

    def __get_primary_key(self):
        keys = [k for k, v in self.items() if v.primary_key]
        if not keys:
            raise self.MissingPrimaryKey

        if len(keys) > 1:
            raise self.MultiplePrimaryKeys

        return keys[0]

    @property
    def mandatory_fields(self):
        return dict(x for x in self.items() if not x[1].opt)

    @property
    def optional_fields(self):
        return dict(x for x in self.items() if x[1].opt)

    @property
    def auto_fields(self):
        return dict(x for x in self.items() if x[1].auto)


class MetaModel(type):
    def __init__(cls, name, bases, dct):
        type.__init__(cls, name, bases, dct)
        if name is 'Model':
            return

        if not dct.has_key('objects'):
            cls.objects = Manager.managers[settings.manager](cls)

        assert isinstance(cls.objects, Manager)
        cls.schema = Schema(sorted(
                getmembers(cls, lambda x: isinstance(x, Field)),
                key=itemgetter(1)))


class Model(object):

    __metaclass__ = MetaModel
    objects = None
    schema = None

    def __init__(self, **register):
        self._store = None

        self.check_unknown_keys(register)
        self.load_register(register)
        self.assure_primary_key()
        self.create_related_api()

    def create_related_api(self):
        for key, field in self.schema.items():
            if isinstance(field, OneToManyField):
                setattr(self,
                        '{0}_set'.format(field.cast.__name__.lower()),
                        RelatedManager(field.cast, self, key))

    def assure_primary_key(self):
        pk_name = self.schema.primary_key
        if isinstance(self.schema[pk_name], AutoField):
            setattr(self, pk_name, None)

    @property
    def pk(self):
        return getattr(self, self.schema.primary_key)

    def set_pk(self, value):
        setattr(self, self.schema.primary_key, value)

    def check_unknown_keys(self, register):
        unknown_fields = field_names(register) - field_names(self.schema)
        if unknown_fields:
            raise exc.UnknownField("{0} got unexpected keyword arguments: {1}".format(
                    self.__class__.__name__, list(unknown_fields)))

    def validate(self):
        missing_fields = [k for k in self.schema.mandatory_fields.keys()
                          if isinstance(getattr(self, k), UndefinedValue)]

        if missing_fields:
            raise exc.MissingMandatoryField(list(missing_fields))

    def load_register(self, register):
#        print register
#        print self.schema.items()
        for key, field in self.schema.items():
#            print '--', key, field

            try:
                raw_value = register[key]
            except KeyError:
                try:
                    raw_value = field.fallback()
                except exc.MissingMandatoryField:
                    setattr(self, key, UndefinedValue(key))
                    continue

            value = field.to_python(raw_value)
            setattr(self, key, value)

    def text_render(self):
        retval = ''
        for k, v in self.schema.items():
            retval += "{0}: {1}\n".format(k, getattr(self, k))

        return retval

    def save(self):
        self.validate()
        self.objects.save(self)

    def __eq__(self, other):
        return all(getattr(self, x) == getattr(other, x) for x in self.schema.keys())

    def __str__(self):
        if hasattr(self, '__unicode__'):
#            print "->", self.__class__.__name__
            return unicode(self).encode('utf-8')
        return "{0} {1}".format(self.__class__.__name__, self.pk)

    def __repr__(self):
        return "<{0}>".format(str(self))


#class Settings:
#    def __init__(self):
#        self.fs = pyarco.fs.ActualFileSystem(os.getcwd())
#
#setting = Settings()
