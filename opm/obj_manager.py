# -*- coding:utf-8; tab-width:4; mode:python -*-

from configobj import ConfigObj

from manager import Manager, FileManager, UndefinedValue
from fields import OneToOneField, OneToManyField, ManyToManyField


#class ObjStore(object):
#    def __init__(self, fpath=None):
#        infile = None if fpath is None else self.FS.open(fpath)
#        self.obj = ConfigObj(infile=infile)
#
#    def save(self, fpath):
#        pass


class ObjManager(FileManager):

    def _store_create(self, fpath=None):
        infile = None if fpath is None else self.FS.open(fpath)
        return ConfigObj(infile=infile)

    def _store_save(self, store, fpath):
        with self.FS.open(fpath, 'w') as fd:
            store.write(fd)

    def _load_1to1_related(self, key, field, store, subject):
        if not store.has_key(key):
            return UndefinedValue(key)

        return field.cast(**store[key])

    def _load_1toM_related(self, key, field, store, subject):
        retval = []
        for pk in store[key].keys():
            instance = field.cast(**store[key][pk])
            instance.set_pk(pk)
            retval.append(instance)

        return retval

    def _load_MtoM_related(self, key, field, store, subject):
        assert isinstance(store[key], list)
        retval = []
        for pk in store[key]:
            instance = field.cast.objects.load(pk)
            retval.append(instance)

        return retval

    def _save_value(self, key, field, store, value):
        store[key] = field.to_store(value)

    def _save_1to1_related(self, key, field, store, subject):
        if subject is None:
            return

        subject.validate()
        store[key] = {}
        self._save_items(store[key], subject)

    def _save_1toM_related(self, key, field, store, instances):
        store[key] = {}
        for subject in instances:
            store[key][subject.pk] = {}
            self._save_items(store[key][subject.pk], subject)

    def _save_MtoM_related(self, key, field, store, instances):
        store[key] = [x.pk for x in instances]
