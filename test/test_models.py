# -*- coding:utf-8; tab-width:4; mode:python -*-

from opm.models import *
from opm.fields import *

class Person(Model):
    nick = TextField(primary_key=True)
    firstname = TextField()
    surname = TextField()

class Address(Model):
    street = TextField()
    number = IntField()

class PersonA(Person):
    nick = TextField(primary_key=True)
    work_address = OneToOneField(Address)

class PersonB(Person):
    nick = TextField(primary_key=True)
    work_address = OneToOneField(Address)
    home_address = OneToOneField(Address, opt=True)

class Education(Model):
    id = TextField(primary_key=True)
    name = TextField()
    entity = TextField()

class PersonC(PersonB):
    education = OneToManyField(Education)

class Paper(Model):
    id = TextField(primary_key=True)
    title = TextField()
    authors = ManyToManyField(Person)
